﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyXwebAuth.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CompanyXwebAuth.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (isUserAdmin())
                {
                    return RedirectToAction("Index", "Employees");
                }
                return RedirectToAction("Index", "Skills");
            }
            return View();
        }
        public bool isUserAdmin()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var role = userManager.GetRoles(user.GetUserId());
                if (role[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        [ChildActionOnly]
        public ActionResult EmployeesMenu()
        {
            if (User.IsInRole("Admin"))
            {
                return PartialView("_EmployeesMenu");
            }
            return new EmptyResult();
        }

        public ActionResult About(string user)
        {
            user = User.Identity.Name;
          //  ViewBag.Message = "Your application description page.";
            ViewBag.Message = "Employee competence evaluation system for CompanyX.";
         
            return View();
        }

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}