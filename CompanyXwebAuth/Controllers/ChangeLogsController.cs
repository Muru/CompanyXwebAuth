﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyXwebAuth;

namespace CompanyXwebAuth.Controllers
{
    public class ChangeLogsController : Controller
    {
        private CompanyXEntities db = new CompanyXEntities();

        // GET: ChangeLogs
        public ActionResult Index(string sortOrder)
        {
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.EmployeeSortParm = String.IsNullOrEmpty(sortOrder) ? "empl_desc" : "";
            ViewBag.ModifierSortParm = String.IsNullOrEmpty(sortOrder) ? "mod_desc" : "";

            var changeLogs = db.ChangeLogs.Include(c => c.Employee).Include(c => c.Modifier);

            switch (sortOrder)
            {
                case "Date":
                    changeLogs = changeLogs.OrderBy(c => c.ChangeTime);
                    break;
                case "empl_desc":
                    changeLogs = changeLogs.OrderBy(c => c.Employee.FullName);
                    break;
                case "mod_desc":
                    changeLogs = changeLogs.OrderBy(c => c.Employee.FullName);
                    break;
                default:
                    changeLogs = changeLogs.OrderByDescending(c => c.ChangeTime);
                    break;
            }

            return View(changeLogs.ToList());
        }

        // GET: ChangeLogs/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ChangeLog changeLog = db.ChangeLogs.Find(id);
        //    if (changeLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(changeLog);
        //}

        // GET: ChangeLogs/Create
        //public ActionResult Create()
        //{
        //    ViewBag.EmployeeId = new SelectList(db.Employees, "IdCode", "FullName");
        //    ViewBag.ModifierId = new SelectList(db.Employees, "IdCode", "FullName");
        //    return View();
        //}

        // POST: ChangeLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Qnr,EmployeeId,ModifierId,ChangeTime")] ChangeLog changeLog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ChangeLogs.Add(changeLog);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.EmployeeId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.EmployeeId);
        //    ViewBag.ModifierId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.ModifierId);
        //    return View(changeLog);
        //}

        // GET: ChangeLogs/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ChangeLog changeLog = db.ChangeLogs.Find(id);
        //    if (changeLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.EmployeeId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.EmployeeId);
        //    ViewBag.ModifierId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.ModifierId);
        //    return View(changeLog);
        //}

        // POST: ChangeLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Qnr,EmployeeId,ModifierId,ChangeTime")] ChangeLog changeLog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(changeLog).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.EmployeeId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.EmployeeId);
        //    ViewBag.ModifierId = new SelectList(db.Employees, "IdCode", "FullName", changeLog.ModifierId);
        //    return View(changeLog);
        //}

        // GET: ChangeLogs/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ChangeLog changeLog = db.ChangeLogs.Find(id);
        //    if (changeLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(changeLog);
        //}

        // POST: ChangeLogs/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ChangeLog changeLog = db.ChangeLogs.Find(id);
        //    db.ChangeLogs.Remove(changeLog);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
