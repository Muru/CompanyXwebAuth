﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyXwebAuth;
using PagedList;
using CompanyXwebAuth.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CompanyXwebAuth.Controllers
{
    public class EmployeesController : Controller
    {
        private CompanyXEntities db = new CompanyXEntities();

        // GET: Employees
        public ViewResult Index(string sortOrder, string searchString, string searchId, string button, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.AdminSortParm = sortOrder == "Admin" ? "admin_desc" : "Admin";
            ViewBag.ActiveSortParm = sortOrder == "Active" ? "active_desc" : "Active";

            if (searchString != null) { page = 1; } else { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString ;

            var employees = from e in db.Employees select e;
            int pageSize = 10;
            int pageNumber = (page ?? 1);

           

            if (!String.IsNullOrEmpty(searchString))
            {
                employees = employees.Where(e => e.FullName.Contains(searchString));
            }
            if (!String.IsNullOrEmpty(searchId))
            {
                employees = employees.Where(e => e.IdCode.Contains(searchId));
            }

            

            switch (sortOrder)
            {
                case "name_desc":
                    employees = employees.OrderByDescending(e => e.FullName);
                    break;
                case "Date":
                    employees = employees.OrderBy(e => e.Adddate);
                    break;
                case "id_desc":
                    employees = employees.OrderBy(e => e.IdCode);
                    break;
                case "Admin":
                    employees = employees.OrderByDescending(e => e.Rights);
                    break;
                case "Active":
                    employees = employees.OrderByDescending(e => e.Active);
                    break;
                default:
                    employees = employees.OrderBy(e => e.FullName);
                    break;
            }

            if ((button ?? "") == "Reset")
            {
                //ModelState.Remove("SearchString");
                ModelState.Clear();
                return View(employees.ToPagedList(pageNumber, pageSize));
            }

            return View(employees.ToPagedList(pageNumber, pageSize));
        }

        // GET: Employees/Details/5
        //public ActionResult Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Employee employee = db.Employees.Find(id);
        //    if (employee == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(employee);
        //}

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdCode,FullName,Active,Adddate,Rights")] Employee employee, string user)
        {
            user = User.Identity.Name;
            ApplicationDbContext context = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (ModelState.IsValid)
            {
                var count =  db.Employees.Count(u => u.IdCode == employee.IdCode);
                if (count == 0)
                {
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    if (employee.Rights)
                    {
                        var adminAccount = new ApplicationUser();
                        adminAccount.UserName = employee.IdCode;
                        adminAccount.FullName = employee.FullName;
                        adminAccount.Email = $"{employee.IdCode}@companyx.com";
                        adminAccount.EmailConfirmed = true;
                        string pass = "Pa$$w0rd";
                        try
                        {
                            var checkAccount = userManager.Create(adminAccount, pass);
                            if (checkAccount.Succeeded)
                            {
                                var addRights = userManager.AddToRole(adminAccount.Id, "Admin");
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        var userAccount = new ApplicationUser();
                        userAccount.UserName = employee.IdCode;
                        userAccount.FullName = employee.FullName;
                        userAccount.Email = $"{employee.IdCode}@companyx.com";
                        userAccount.EmailConfirmed = true;
                        string pass = "Pa$$w0rd";
                        try
                        {
                            var checkAccount = userManager.Create(userAccount, pass);
                            if (checkAccount.Succeeded)
                            {
                                var addRole = userManager.AddToRole(userAccount.Id, "User");
                            }
                        }
                        catch { }
                    }

                    Helpers.Helpers.WriteChangeLog(employee.IdCode, "Add employee", user);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Message = "Employee already exists!";
                }                             
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(string id)
        {
            if(User.Identity.IsAuthenticated)
            { 
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee = db.Employees.Find(id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                return View(employee);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdCode,FullName,Active,Adddate,Rights")] Employee employee, string user)
        {
            user = User.Identity.Name;
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                Helpers.Helpers.WriteChangeLog(employee.IdCode, "Edit employee", user);
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee = db.Employees.Find(id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                return View(employee);
            }
            else
            {
                return HttpNotFound();
            }
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id, string user)
        {
            user = User.Identity.Name;
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            Helpers.Helpers.WriteChangeLog(id, "Delete employee", user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
