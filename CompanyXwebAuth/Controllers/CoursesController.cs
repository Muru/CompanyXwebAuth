﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyXwebAuth;
using System.IO;
using System.Text;
using System.Drawing;
using CompanyXwebAuth.Models;

namespace CompanyXwebAuth.Controllers
{
    public class CoursesController : Controller
    {
        private CompanyXEntities db = new CompanyXEntities();
        
        // GET: Courses
        public ActionResult Index(string sortOrder, string searchName, string searchTitle, string button)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.TypeSortParm = String.IsNullOrEmpty(sortOrder) ? "type_desc" : "";
            ViewBag.CourseSortParm = String.IsNullOrEmpty(sortOrder) ? "course_desc" : "";
            ViewBag.SkillSortParm = String.IsNullOrEmpty(sortOrder) ? "skill_desc" : "";
            ViewBag.PassedSortParm = sortOrder == "Passed" ? "passed_desc" : "Passed";

            var courses = db.Courses.Include(c => c.Category).Include(c => c.Employee);
         

            if ((button ?? "") == "Reset")
            {
                ModelState.Clear();
                return View(courses.ToList());
            }

            if (!String.IsNullOrEmpty(searchName))
            {
                courses = courses.Where(s => s.Employee.FullName.Contains(searchName));
            }
            if (!String.IsNullOrEmpty(searchTitle))
            {
                courses = courses.Where(s => s.CourseName.Contains(searchTitle));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    courses = courses.OrderByDescending(c => c.Employee.FullName);
                    break;
                case "type_desc":
                    courses = courses.OrderBy(c => c.CourseType);
                    break;
                case "course_desc":
                    courses = courses.OrderBy(c => c.CourseName);
                    break;
                case "skill_desc":
                    courses = courses.OrderBy(c => c.Category.CategoryName);
                    break;
                case "Passed":
                    courses = courses.OrderBy(c => c.Passed);
                    break;
                default:
                    courses = courses.OrderBy(c => c.Passed);
                    break;
            }
                    return View(courses.ToList());
        }

        // GET: Courses/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Course course = db.Courses.Find(id);
        //    if (course == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(course);
        //}

        // GET: Courses/Create
        public ActionResult Create()
        {
            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName");
            ViewBag.IdCode = new SelectList(db.Employees.OrderBy(x => x.FullName), "IdCode", "FullName");
            ViewBag.UserId = db.Employees.Where(x => x.IdCode == User.Identity.Name).First().FullName; 

            var TypeList = new List<string>();
            var TypeQry = from b in db.Courses
                           orderby b.CourseType
                           select b.CourseType;
            TypeList.AddRange(TypeQry.Distinct());
            ViewBag.CourseType = new SelectList(TypeList);

            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Qnr,IdCode,CourseName,CourseType,SkillID,Textfield,Passed,DataFile")] Course course, HttpPostedFileBase picture, string user)
        {
            user = User.Identity.Name;
            if (!User.IsInRole("Admin"))
            {
                course.IdCode = db.Employees.Where(x => x.IdCode == user).First().IdCode;
            }
            if (ModelState.IsValid)
            {
                if (picture != null)
                {
                    if (picture.ContentLength > 0)
                    {
                        string _PicName = Path.GetFileName(picture.FileName);
                        string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _PicName);

                        picture.SaveAs(_path);
                        course.DataFile = System.IO.File.ReadAllBytes(_path);
                    }
                }
                db.Courses.Add(course);
                db.SaveChanges();
                Helpers.Helpers.WriteChangeLog(course.IdCode, "Add course/exam", user);
                return RedirectToAction("Index");
            }

            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", course.SkillID);
            ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", course.IdCode);
            var TypeList = new List<string>();
            var TypeQry = from b in db.Courses
                          orderby b.CourseType
                          select b.CourseType;
            TypeList.AddRange(TypeQry.Distinct()); 
            ViewBag.Types = new SelectList(TypeList);

            return View(course);
        }
         
        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Course course = db.Courses.Find(id);
                if (course == null)
                {
                    return HttpNotFound();
                }
                ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", course.SkillID);
                ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", course.IdCode);
                return View(course);
            }
            else return HttpNotFound();
            
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Qnr,IdCode,CourseName,CourseType,SkillID,Textfield,Passed,DataFile")] Course course, HttpPostedFileBase picture, string user)
        {
            user = User.Identity.Name;
            if (ModelState.IsValid)
            {
                if (picture != null)
                {
                    if (picture.ContentLength > 0)
                    {
                        string _PicName = Path.GetFileName(picture.FileName);
                        string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _PicName);

                        picture.SaveAs(_path);
                        course.DataFile = System.IO.File.ReadAllBytes(_path);
                    }
                }
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                Helpers.Helpers.WriteChangeLog(course.IdCode, "Edit course/exam", user);
                return RedirectToAction("Index");
            }
            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", course.SkillID);
            ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", course.IdCode);
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Course course = db.Courses.Find(id);
                if (course == null)
                {
                    return HttpNotFound();
                }
                return View(course);
            }
            else
            {
                return HttpNotFound();
            }
            
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string user)
        {
            user = User.Identity.Name;
            Course course = db.Courses.Find(id);
            var employeeToChange = course.IdCode;
            db.Courses.Remove(course);
            db.SaveChanges();
            Helpers.Helpers.WriteChangeLog(employeeToChange, "Delete course/exam", user);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
