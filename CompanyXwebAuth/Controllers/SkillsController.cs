﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyXwebAuth;
using PagedList;

namespace CompanyXwebAuth.Controllers
{
    public class SkillsController : Controller
    {
        private CompanyXEntities db = new CompanyXEntities();

        // GET: Skills
        public ActionResult Index(string sortOrder, string searchName, string searchSkill, string searchLevel, string button)
        {
           
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.SkillSortParm = String.IsNullOrEmpty(sortOrder) ? "skill_desc" : "";

            ViewData["Courses"] = db.Courses;

            var skills = db.Skills.OrderBy(x => x.Employee.FullName).Include(s => s.SkillCategory).Include(s => s.Employee);

            ViewBag.SkillList = new SelectList(db.Categories, "SkillID", "CategoryName", searchSkill);


            var SkillList = new List<string>();
          
            var SkillQry = from d in db.Categories
                           orderby d.CategoryName
                           select d.CategoryName;

            SkillList.AddRange(SkillQry.Distinct());
          
            ViewBag.searchSkill = new SelectList(SkillList);

            var LevelList = new List<string>();
            
            var LevelQry = from b in db.Skills
                           orderby b.Mastery
                           select b.Mastery;

            LevelList.AddRange(LevelQry.Distinct());
            ViewBag.searchLevel = new SelectList(LevelList);


            if ((button ?? "") == "Reset")
            {
                ModelState.Clear();

                SkillList = new List<string>();
                SkillQry = from d in db.Categories
                           orderby d.CategoryName
                           select d.CategoryName;

                SkillList.AddRange(SkillQry.Distinct());
                ViewBag.searchSkill = new SelectList(SkillList);

                LevelList = new List<string>();
                LevelQry = from b in db.Skills
                           orderby b.Mastery
                           select b.Mastery;

                LevelList.AddRange(LevelQry.Distinct());
                ViewBag.searchLevel = new SelectList(LevelList);
                return View(skills.ToList());
            }


            if (!String.IsNullOrEmpty(searchName))
            {
                skills = skills.Where(s => s.Employee.FullName.Contains(searchName));
            }
            if (!String.IsNullOrEmpty(searchSkill))
            {
                skills = skills.Where(s => s.SkillCategory.CategoryName == searchSkill);
            }
            if (!String.IsNullOrEmpty(searchLevel))
            {
                skills = skills.Where(s => s.Mastery == searchLevel);
            }


            switch (sortOrder)
            {
                case "name_desc":
                    skills = skills.OrderByDescending(s => s.Employee.FullName);
                    break;
                case "skill_desc":
                    skills = skills.OrderBy(s => s.SkillID);
                    break;
                default:
                    skills = skills.OrderBy(s => s.Employee.FullName);
                    break;
            }

            return View(skills.ToList());
        }

        [ChildActionOnly]
        public ActionResult AdminAction()
        {
            if (User.IsInRole("Admin"))
            {                
                return PartialView("_AdminAction");
            }
            return new EmptyResult();
        }

        [ChildActionOnly]
        public ActionResult CreateNew()
        {
            if (User.IsInRole("Admin"))
            {
                return PartialView("_CreateNew");
            }
            return new EmptyResult();
        }

        // GET: Skills/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Skill skill = db.Skills.Find(id);
        //    if (skill == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(skill);
        //}

        // GET: Skills/Create
        public ActionResult Create()
        {
            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName");
            ViewBag.IdCode = new SelectList(db.Employees.OrderBy(x => x.FullName), "IdCode", "FullName");
            var LevelList = new List<string>();
            var LevelQry = from b in db.Skills
                       orderby b.Mastery
                       select b.Mastery;

            LevelList.AddRange(LevelQry.Distinct());
            ViewBag.Mastery = new SelectList(LevelList);

            return View();
        }

        // POST: Skills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Qnr,IdCode,SkillID,Mastery")] Skill skill, string user)
        {
            user = User.Identity.Name;
            if (ModelState.IsValid)
            {
                db.Skills.Add(skill);
                db.SaveChanges();
                Helpers.Helpers.WriteChangeLog(skill.IdCode, "Add skill", user);
                return RedirectToAction("Index");
            }

            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", skill.SkillID);
            ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", skill.IdCode);
            return View(skill);
        }

        // GET: Skills/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Skill skill = db.Skills.Find(id);
                if (skill == null)
                {
                    return HttpNotFound();
                }
                ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", skill.SkillID);
                ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", skill.IdCode);
                var LevelList = new List<string>();
                var LevelQry = from b in db.Skills
                               orderby b.Mastery
                               select b.Mastery;

                LevelList.AddRange(LevelQry.Distinct());
                ViewBag.Mastery = new SelectList(LevelList);
                return View(skill);
            }
            else
            {
                return HttpNotFound();
            }
            
        }

        // POST: Skills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Qnr,IdCode,SkillID,Mastery")] Skill skill, string user)
        {
            user = User.Identity.Name;
            if (ModelState.IsValid)
            {
                db.Entry(skill).State = EntityState.Modified;
                db.SaveChanges();
                Helpers.Helpers.WriteChangeLog(skill.IdCode, "Edit skill", user);
                return RedirectToAction("Index");
            }
            ViewBag.SkillID = new SelectList(db.Categories, "SkillID", "CategoryName", skill.SkillID);
            ViewBag.IdCode = new SelectList(db.Employees, "IdCode", "FullName", skill.IdCode);
            return View(skill);
        }

        // GET: Skills/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Skill skill = db.Skills.Find(id);
        //    if (skill == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(skill);
        //}

        // POST: Skills/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Skill skill = db.Skills.Find(id);
        //    db.Skills.Remove(skill);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
