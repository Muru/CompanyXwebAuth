﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Owin;
using CompanyXwebAuth.Models;

[assembly: OwinStartupAttribute(typeof(CompanyXwebAuth.Startup))]
namespace CompanyXwebAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();            
        }
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CompanyXEntities CompX = new CompanyXEntities();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("User"))
            {
                var role = new IdentityRole();
                role.Name = "User";
                roleManager.Create(role);
            }

            foreach (var employee in CompX.Employees)
            {
                if (employee.Rights)
                {
                    var adminAccount = new ApplicationUser();
                    adminAccount.UserName = employee.IdCode;
                    adminAccount.FullName = employee.FullName;
                    adminAccount.Email = $"{employee.IdCode}@companyx.com";
                    adminAccount.EmailConfirmed = true;
                    string pass = "Pa$$w0rd";
                    try
                    {
                        var checkAccount = userManager.Create(adminAccount, pass);
                        if (checkAccount.Succeeded)
                        {
                            var addRights = userManager.AddToRole(adminAccount.Id, "Admin");
                        }
                    }
                    catch { }
                }
                else
                {
                    var userAccount = new ApplicationUser();
                    userAccount.UserName = employee.IdCode;
                    userAccount.FullName = employee.FullName;
                    userAccount.Email = $"{employee.IdCode}@companyx.com";
                    userAccount.EmailConfirmed = true;
                    string pass = "Pa$$w0rd";
                    try
                    {
                        var checkAccount = userManager.Create(userAccount, pass);
                        if (checkAccount.Succeeded)
                        {
                            var addRole = userManager.AddToRole(userAccount.Id, "User");
                        }
                    }
                    catch { }
                }
            }
        }
    }
}
