﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyXwebAuth.Helpers
{
    public static class Helpers
    {
        public static void WriteChangeLog(string changable, string op, string modifier)
        {
            CompanyXEntities db = new CompanyXEntities();

            db.ChangeLogs.Add(
                new ChangeLog
                {
                    ChangeTime = DateTime.Now,
                    ModifierId = modifier,
                    EmployeeId = changable,
                    Operation = op
                }
                );
            db.SaveChanges();

        }


    }
}